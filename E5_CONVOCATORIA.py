#AUTOR: : HERNÁNDEZ   LÓPEZ   RAÚL
#CORREO:  freeenergy1975@gmail.com
#FECHA : 16  de   enero  del  2021

 # TEMA:   EN    EL   TECNOLOGICO   NACIONAL  DE   MEXICO   SE  PUBLICO  UNA
 # CONVOCATORIA DE  ESTUDIOS  EN  EL  EXTRANJERO, PODRAN PARTICIPAR AQUELLOS
 # ESTUDIANTES  QUE TENGAN UN  PROMEDIO  MAYOR  A  90. LOS ALUMNOS QUE HAYAN
 # ALCANZADO LA CALIFICACION ENTRARAN  DIRECTO  A LA TOMBOLA PARA EL SORTEO.
 # LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRAN PARTICIPAR EN LA SIGUIENTE
 # CONVOCATORIA SI APRUEBAN EL EXAMEN  DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
 # Y MEJORAN EN UN 20% SU PROMEDIO.

calificacionExamen = []
nombres = []
calificaciones = []
calificacionesSegundaConvocatoria = []

numeroAlumnos = int(input("\nNumero de alumnos: "))

for x in range (0, numeroAlumnos, 1) :
    opcion = True
    while opcion == True:
       nombre = input("\nNombre: ")
       nombres.append(nombre)
       calificacion = int(input("\nCalifacion: "))
       calificaciones.append(calificacion)
       opcion = False
       if calificacion >= 0 and calificacion <= 100 :
            if calificacion > 90 :
                print("\naplica para la convocatoria.\n",
                      "\n____________________________\n")
            else :
                print("\nNo aplica para la primera convocatoria.\n",
                      "\n_______________________________________\n")
       else :
            opcion = True

print("\n**********************\n",
      "\nSegunda convocatoria",
      "\n**********************\n")

for x in range (0, numeroAlumnos, 1) :
    if calificaciones[x] <= 90 and calificaciones[x] >= 70:
        print("\n\nAlumno: ", nombres[x])  
        calificacionExam = int(input("\nCalificacion de examen de idiomas: "))
        calificacionExamen.append(calificacionExam)
        if calificacionExamen[x] > 59:
            califSegSemestre = int(input("\nCalificacion del semestre actual: "))
            calificacionesSegundaConvocatoria.append(califSegSemestre)
            comparacionCalificaciones = calificaciones[x] + (calificaciones[x] * 0.20)
            if calificacionesSegundaConvocatoria[x] >= comparacionCalificaciones or calificacionesSegundaConvocatoria[x] > 90:
                print("\nAplica para la convocatoria.",
                      "\n___________________________\n")
            else:
                print("\nNo aplica.",
                      "\n__________\n")
        else:
            print("\nNo Aplica.",
                  "\n__________\n")

                    

