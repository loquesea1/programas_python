# AUTOR:  Hernandez   Lopez   Raul
# correo: freeenergy1975@gmail.com
# fecha:  9   de  enero  del  2021
# Tema: Elabora un programa con condicionales anidados que solicite 3 calificaciones, 
# obtén el promedio  de  esas  tres  calificaciones, y de acuerdo al promedio que se 
# obtuvo, coloca la leyenda que le corresponde.

sumaCalificaciones = 0

print("\nA continuacion ingresa 3 calificaciones\npara determinar tu promedio",
        "\nEn un rango del 1 al 100")

#Captura las tres calificaciones
for x in range (0, 3, 1):
    ciclo = True
    while ciclo == True:
        calificacion = int(input("\nCalificacion :"))
        #Evalua que el valor se encuentre dentro del rango establecido
        if calificacion >= 0  and calificacion <= 100:
            ciclo = False
            sumaCalificaciones += calificacion 
        #De no ser asi se repite el proceso de la calificacion actual      
        else: 
            ciclo = True
            print("\nEl valor ingresado esta fuera de rango\n")
#Determina el valor del promedio.
promedio = sumaCalificaciones/3
#Evalua cada situacion.
if promedio >= 95 and promedio <= 100:
    print("\nEres Exelete", "\n______________\n") 
elif promedio >= 85 and promedio <= 94:
    print("\nEres Notable", "\n______________\n")
elif promedio >= 75 and  promedio <= 84:
    print("\nEres Bueno", "\n______________\n")
elif promedio >= 70 and promedio <= 74:
    print("\nEres Sufuciente", "\n_____________\n")
else :
    print("\nNA (noalcanza)", "\n_______________\n")



