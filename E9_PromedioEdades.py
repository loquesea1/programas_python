# AUTOR:  Hernandez   Lopez   Raul
# correo: freeenergy1975@gmail.com
# fecha:  9   de  enero  del  2021
# Tema:CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE

#almacena el valor de la suma de las edades.
edades = 0

numeroAlumnos = int(input("Numero de alumnos: "))
#Entrada de datos.
for x in range(0, numeroAlumnos, 1):
    edad = int(input("Edad: "))
    edades += edad
#Determina el promedio de las edades.
promedio = int(edades / numeroAlumnos)
#Impresion ed resultados.
print("\nEl promedio de edades es de: ", promedio)


