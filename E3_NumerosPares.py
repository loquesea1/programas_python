#AUTOR: :  HERNÁNDEZ   LÓPEZ   RAÚL
#CORREO: freeenergy1975@gmail.com
#FECHA : 16  de  enero  del  2021

#Tema: DESARROLLA UN  PROGRAMA  SOLICITA  QUE  SEAN  CAPTURADOS  TRES   
#DATOS NUMERICOS  Y  A  PARTIR DE ELLOS IMPRIMIRA SI EL NUMERO ES PAR.

print("\nA continuacion  se  te solicitaran",
        "\n3 numero para evaluar si son pares",
        "\no no.\n")

for x in range(0, 3, 1) :
    numero = int(input("\nInrgresa un numero entero positivo: "))
    if ( numero % 2 == 0 ) :
        print("\nEl numero es par." 
               ,"\n*****************\n")
    else :    
        print("\nEl numero es impar." 
               ,"\n*******************\n")
