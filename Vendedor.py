#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#Miercoles 28 de octubre del 2020

#Un vendedor recibe un sueldo base más un 10% extra por comisión de sus ventas
#el vendedor desea saber cuánto dinero obtendrá por concepto de comisiones por
#las tres ventas que realiza en el mes y el total que recibirá en el mes
#tomando en cuenta su sueldo base y comisiones.

#Declaracion de variables
comision = 0
sueldo_Total = 0
#Recaba datos
sueldoFijo = float(input("Ingresa el monto de tu sueldo actual $"))
numeroVentas = int(input("Ingresa el numero de ventas que realizaste :")) 
#Determina el monto correspondiente por concepto de ventas 
for x in range (0, numeroVentas, 1):
    venta = float(input("Monto de la venta $"))
    comision =+ (venta * 0.10)
    
#Calcula el presupueto correspondiente
sueldoTotal = comision + sueldoFijo;
#Imprime resultados
print("El monto recabado por concepto de ventas es de [$"
, comision , "]")
print("Sueldo total [$" , sueldoTotal , "]")
