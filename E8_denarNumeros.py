# AUTOR:  Hernandez   Lopez   Raul
# correo: freeenergy1975@gmail.com
# fecha:  9   de  enero  del  2021
# Tema: Elabora un programa que compare tres numeros y los ordene de mayor a menor
#Declaracion de variables.
ordenar = []
cambioPosicion = 0
#Entrada de datos
for x in range(0, 3, 1):
    numero = int(input("Numero: "))
    ordenar.append(numero)
#Ordenamiento de datos
for x in range(0, 4, 1):
    for y in range(0, 2, 1):
        if ordenar[y] > ordenar[y+1]:
            cambioPosicion = ordenar[y]
            ordenar[y] = ordenar[y+1]
            ordenar[y+1] = cambioPosicion
#Impresion de resultados.
for x in range(0, 3, 1):
    print(ordenar[x])
