#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#16 de octubre del 2020

#Calcular el nuevo salario de un empleado si obtuvo un
#incremento del 8% sobre su salario actual y un
#descuento de 2.5% por servicios.

#Declaracion de variables
Salario_Final = 0
Salario_Actual = 0
Nuevo_Salario = 0
Descuento = 0

Salario_Actual = float(input("\nSalario actual :"))
Nuevo_Salario = Salario_Actual + (Salario_Actual * 0.08) 
Descuento = Nuevo_Salario * 0.025
Salario_Final = Nuevo_Salario - Descuento
#Impresion de resulatdos
print("\nSe te otorgo un aumento del 8%\n\nSin embargo se te descontaran 2.5% por servicios")
print("\nMonto libre a cobrar [$", Salario_Final, "]")
