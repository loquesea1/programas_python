#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#20 de octubre del 2020

#Obtener la edad de una persona en meses, si se ingresa su edad en años y meses.
#Ejemplo: Ingresado 3 años 4 meses debemostrar 40 meses

#Declaracion de variables
Meses = 0
Años = 0
Edad_Meses = 0
#Recopila datos
Años = int(input("\nIngresa tu edad en Años :"))
Meses = int(input("\nIngresa los meses restantes "))
#Define el numero de meses en los años y los suma con los meses restantes
Edad_Meses = (Años * 12) + Meses
#Imprime resultados
print("\nEdad en meses :", Edad_Meses)

