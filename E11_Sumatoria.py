#AUTOR: : HERNÁNDEZ   LÓPEZ   RAÚL
#CORREO:  freeenergy1975@gmail.com
#FECHA : 23  de   enero  del  2021
#Tema: Genera la sumatoria de un numero.
sumatoria = 1
#Recibe el valor del numero para determinar el factorial
numero = int(input("Ingresa un numero: "))
#Dtermina el valor del factorial del numero que se ingreso
for x in range(1, numero+1, 1):
    sumatoria += x;
#Impresion de resultados
print("La sumatoria de ", numero, " es ", sumatoria)
