#Autor: Hernandez Lopez Raul @Neo
#correo: freeenergy1975@gmail.com
#fecha: 16 de enero del 2021

# TEMA :EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
# TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE
# FRECUENTE. LES OTORGARAN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.

#Declracion de variables y arreglos. 
listaDeCompras = []
preciosDeCompra = []
sumaPrecios = 0
montoTotal = 0
opciones = bool(True)


numeroCompras = int(input("\ningrese el numero de compras que realizo: "))

for x in range(0, numeroCompras, 1):
    #Recoleccion de datos.
    nombreCompra = (input("\nNombre del producto: "))
    precioCompra = float(input("\nprecio del producto $"))
    print("______________________________");
    #Introduccion de datos obtenidos a un arreglo.
    listaDeCompras.append(nombreCompra)
    preciosDeCompra.append(precioCompra)
    #Sumatoria del precio de la lista de compras.
    sumaPrecios += precioCompra


while opciones == True:        
    tarjeta = int(input("\nCuenta con tarjeta de cliente frecuente\n1)yes\n0)no: "))
    #Evalua la situacion.
    if tarjeta >= 0 and tarjeta < 2: 
        opciones = False
        if tarjeta == 1:
            montoTotal = sumaPrecios * 0.80
            print("\n\nPrecio:\tDescuento\tMonto a pagar:\n"
               , sumaPrecios, "\t20%\t\t", montoTotal
               ,"\n********************************************\n")
        else :
            print("\n\nPrecio:\tDescuento\tMonto a pagar:\n"
               , sumaPrecios, "\t0%\t\t", sumaPrecios
               ,"\n**************************************\n")
            opciones = False
    else :
        print("\nIngrese una opcion valida\n")
        opciones = True




