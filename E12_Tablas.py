#AUTOR: : HERNÁNDEZ   LÓPEZ   RAÚL
#CORREO:  freeenergy1975@gmail.com
#FECHA : 23  de   enero  del  2021
#Tema: Imprime la tabla de multiplicar de un numero N.

tabla = int(input("\nIngresa la tabla de multiplicar: "))


for x in range(1, 11, 1):
    #realiza la multiplicacion
    multiplicacion = tabla * x
    #Imprime resultados
    print(tabla, " x ", x, " = ", multiplicacion)

