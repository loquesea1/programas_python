#RAÚL_HERNÁNDEZ_LÓPEZ
#freeenrgy1975@gmail.com
#17 de octubre del 2020

#Calcular el nuevo salario de un empleado si se le 
#descuenta el 20% de su salario actual.

#Declaracion de variables
Salario_Actual = 0
Salario_Descuento = 0
Descuento = 0
#Calcula el valor del descuento y el sueldo con el descuento
Salario_Actual = float(input("Salario actual $"))
Salario_Descuento = Salario_Actual * 0.80;
Descuento = Salario_Actual * 0.20;
#imprime resultados
print("\nEl descuento es igual a $", Descuento 
, "\nMonto a cobrar $", Salario_Descuento)



