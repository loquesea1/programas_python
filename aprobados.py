#Raul_Hernandez_Lopez
#freeenergy1975@gmail.com
#Miercoles 17 de septiembre del 2020

#Este programa te permite calcular almacenar una cantidad N de calificaciones y nombres
#de alumnos y clasificarlos en aprobados y no aprobados.

#Declaracion de variables

calificacion = 0 
Aprobados = 0
NoAprobados = 0
numero_alumnos = 0
posicion = 0
nombres = []
calificaciones = []
numero_alumnos = int(input("\nIngresa el numero de alumnos a evaluar :"))


#Recopilacon de datos.
for x in range(0, numero_alumnos , 1):
    posicion = posicion + 1
    print("Nombre del alumno [", posicion, "] :")
    nombre=(input())
    nombres.append(nombre)
    calificacion = float(input("Calificacion :"))
    calificaciones.append(calificacion)
    #Proceso para definir si el alumno aprueba o no 
    if calificaciones[x] >= 0 and calificaciones[x] < 6 :
        NoAprobados = NoAprobados + 1 
        print("\nEl alumno ", nombres[x], " no aprobo")
    elif calificaciones[x] >= 6 and calificaciones[x] <= 10 :
        Aprobados = Aprobados + 1
        print("\nEl alumno ", nombres[x], " aprobo con una calificacion de [", calificaciones[x],"]")
     
#Impresion de resultados
print("\nEL NUMERO DE APROBADOS ES DE [", Aprobados, "]")
print("\nEL NUMERO DE REPROBADOS ES DE [", NoAprobados, "]")   
