
##########################################################################
#AUTOR: : HERNÁNDEZ   LÓPEZ   RAÚL                                       #
#CORREO:  freeenergy1975@gmail.com                                       #
#FECHA : 26  de   enero  del  2021                                       #
#Tema:                                                                   #
# EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER #
# MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O #
# HAN TENIDO  ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN #
# HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:                 #
# EDAD.                                                                  #
# LOCALIDAD.                                                             #
# TIENEN INTERNET EN CASA.                                               #
# TIENEN COMPUTADORA EN CASA.                                            #
# CUENTA CON LOS RECURSOS  BÁSICOS NECESARIOS DE ALIMENTACION. RECOLECTA #
# LOS DATOS Y PRESENTA  LOS RESULTADOS CONSIDERANDO  QUE SE DESCONOCE LA #
# CANTIDAD DE ALUMNOS IMPRIME  LOS  RESULTADOS CON NÚMEROS Y PORCENTAJES.#
##########################################################################

#Declaracion de variables contadores.
mayorEdad = 0
menorEdad = 0
sinInternet = 0 
conInternet = 0
conComputadora = 0
sinComputadora = 0
buenaAlimentacion = 0
malaAlimentacion = 0
jilotepec = 0
sanVicente = 0
danxho = 0
fresno = 0
otro = 0
x = 0

respuesta = 1

while respuesta == 1 : 
    x += 1
    #Entrada de datos.
    edad = int(input("\nEdad: "))
    if edad > 18 :
        mayorEdad += 1
    else :
        menorEdad += 1

    internet = int(input("\nCuentas con internet en casa?\n1)yes 0)No: "))
    if internet == 1 :
        conInternet += 1
    else :    
        sinInternet += 1

    computadora = int(input("\nCuentas con computadora en casa?\n1)yes 0)no: "))
    if computadora == 1 :
        conComputadora += 1
    else :
        sinComputadora += 1

    alimentacion = int(input("\nCosideras que tu alimentacion es buena\n 1)yes 0)No: "))
    if alimentacion == 1 :
        buenaAlimentacion += 1
    else :    
        malaAlimentacion += 1

    print("\nLocalidad :", "\n1)Jilotepec\n2)San Vicente\n3)Danxho\n4)Fresno\n5)otro", 
            "\nDigita tu opcion: ")
    localidad = int(input())
    #Evaluacion localidad
    if localidad == 1 :
        jilotepec += 1
    elif localidad == 2 :
        sanVicente += 1
    elif localidad == 3 :
        danxho += 1
    elif localidad == 4 :
        fresno += 1
    else :
        otro += 1
    respuesta = int(input("\nContinuar\n1)yes 2)No: "))

      
porcentaje = 100/x
porcenComp = porcentaje * conComputadora
porcenSinComp = porcentaje * sinComputadora
porcenInter = porcentaje * conInternet
porcenSinInter = porcentaje * sinInternet
porcenMayorEdad = porcentaje * mayorEdad
porcenMenorEdad = porcentaje * menorEdad
porcenBuenaAlimentacion = porcentaje * buenaAlimentacion
porcenMalaAlimentacion = porcentaje * malaAlimentacion
porcenJilotepec = porcentaje * jilotepec
porcenSanVicente = porcentaje * sanVicente
porcenDanxho = porcentaje * danxho
porcenFresno = porcentaje * fresno
porcenOtro = porcentaje * otro

print("\nResultados           |          Numero de alumnos: ", x,
        "\n_____________________________________________________\n",
        "\nDescripcion\t\t\tAlumnos\t\t%",
        "\nCuentan con computadora", "\t", conComputadora, "\t\t", porcenComp,
        "\nNo tienen computadora  ", "\t", sinComputadora, "\t\t", porcenSinComp,
        "\ncuentan con internet   ", "\t", conInternet, "\t\t", porcenInter,
        "\nNo cuentan con internet", "\t", sinInternet, "\t\t", porcenSinInter,
        "\nMayores de edad        ", "\t", mayorEdad, "\t\t", porcenMayorEdad,
        "\nMenores de edad        ", "\t", menorEdad, "\t\t", porcenMenorEdad,
        "\nTienen buena alimentacion", "\t", buenaAlimentacion, "\t\t", porcenBuenaAlimentacion,
        "\nTienen mala alimentacion", "\t", malaAlimentacion, "\t\t", porcenMalaAlimentacion)
print("\n_____________________________________________________\n",
        "Localidad\t\tAlumnos\t\t%", 
        "\n_____________________________________________________\n"
        "\nJilotepec  \t\t", jilotepec, "\t\t", porcenJilotepec,
        "\nSan Vicente\t\t", sanVicente, "\t\t", porcenSanVicente,
        "\nDanxho     \t\t", danxho, "\t\t", porcenDanxho,
        "\nFresno     \t\t", fresno, "\t\t", porcenFresno,
        "\nOtro        \t\t", otro, "\t\t", porcenOtro, "\n")    
