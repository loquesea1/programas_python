#AUTOR: : HERNÁNDEZ   LÓPEZ   RAÚL
#CORREO:  freeenergy1975@gmail.com
#FECHA : 23  de   enero  del  2021
#Tema: Genera el factorial de un número.
factorial = 1
#Recibe el valor del numero para determinar el factorial
numero = int(input("Ingresa un numero: "))
#Dtermina el valor del factorial del numero que se ingreso
for x in range(1, numero+1, 1):
    factorial *= x;
#Impresion de resultados
print("El factorial de ", numero, " es ", factorial)
