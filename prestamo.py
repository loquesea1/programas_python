#Raul_Hernandez_Lopez
#freeenergy1975@gmail.com
#15 de octubre del 2020

#Una persona recibe un préstamo de $10,000.00 de un banco y desea 
#saber cuánto pagará de interés, si el banco le cobra una tasa del
#27% anual.

Tasa_Interes = 0.27

Fecha_Prestamo = int(input("\nFecha de prestamo :"))
Fecha_Actual = int(input("\nFecha actual :"))
Prestamo = float(input("\nCantidad prestada $"))
Tiempo_Transcurrido = Fecha_Actual - Fecha_Prestamo

for x in range(1, Tiempo_Transcurrido, 1):
   Fecha_Prestamo += 1
   Prestamo = Prestamo + (Prestamo * Tasa_Interes) 
   print ("\nAño [", Fecha_Prestamo, "]\nMonto a pagar [$", Prestamo, "]")
