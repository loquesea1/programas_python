#Raul_Hernandez_Lopez
#freeenergy1975@gmail.com
#Viernes 02 de octubre del 2020

#Calcula el precio de un boleto en funcion de los kilometros si se sabe que 
#el costo por kilometro es de 20.5

#Declaracion de viables 
kilometros = 20.5
costo = 0
distancia = 0
#recopilacion de valores 
print("\ningresa a que distancia en kilometros se encuentra tu destino :")
distancia= float(input())
#cacula el costo del boleto
costo = kilometros * distancia
#impresion de resultados
print("El costo de tu boleto es de [$", costo, "]\n")
