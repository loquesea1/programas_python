#Raul hernandez lopez(Berserker)
#freeenergy1975@gmail.com
#Vierner 04 de septiembre del 2020

#este programa permite almacenar un numero N de ventas y separarla de acuerdo
#a una serie de condiciones: ventas mayores a 10,000 pero menores de 20,000
#Ventas iguales o menores de 10,000... cuanto es el monto de cada una y el monto 
#global.
MontoGlobal= 0 
sumaMayor = 0
sumaMenor = 0
posicion = 0
menor = 0
mayor = 0
ventas = []

numero = int(input("\nCual es el numero de ventas? :"))

for x in range(0, numero, 1):
    posicion = posicion + 1
    print("\nIngresa el valor de la venta No. ", posicion, " :")
    val=int(input())
    ventas.append(val)
    #almacena el total de ventas realizadas.
    MontoGlobal = MontoGlobal + ventas[x]
    #Antes de que el ciclo continue, tiene que pasar un filtro de dos condicionales
    if ventas[x] >= 0 and ventas[x] <= 10000:
        sumaMenor = sumaMenor + ventas[x]
        menor = menor + 1
    elif ventas[x] > 10000 and ventas[x] <= 20000:
        sumaMayor = sumaMayor + ventas[x]
        mayor = mayor + 1

#impresion de resultados 
print("\nRealizaste", menor, "ventas menores a 10,000 y la suma de esta es de :", sumaMenor)
print("\nRealizaste", mayor, "ventas mayores a 10,000 y la suma de estas es de :", sumaMayor)
print("\nMientras que el monto global es de ",MontoGlobal, "\n")     
